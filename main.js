"use strict"

let userInput = prompt("Enter a number please");
let number = Number(userInput);
let hasNumber = false;

while ( isNaN(number) || !Number.isInteger(number) || number < 0 ) {
    alert("Enter a proper number please");
    number = Number(prompt("Enter a number"));
}

for (let i = 0; i <= number; i++) {
    if (i % 5 === 0 && i !== 0) {
        console.log(i);
        hasNumber = true;
    }
}

if (!hasNumber) {
    console.log("Sorry, no numbers");
}

